﻿using UnityEngine;

public class CardDeck : MonoBehaviour {
    private Card currentCard;
    public GameObject cardPrefab;

    public delegate void OnCardEvent(Card card);

    public static event OnCardEvent onCardExecute;
    public static event OnCardEvent onCardSave;

    void Start() {
        CardGenerator.initialize();
        CardGenerator.cardGeneratedDelegate += createCard;
    }

    public void createCard(CardData cardData) {
        removeCurrentCard();

        GameObject newCard = Instantiate(cardPrefab, transform);
        currentCard = newCard.GetComponent<Card>();
        currentCard.initialize(cardData);
    }

    private void removeCurrentCard() {
        if (currentCard != null) {
            Destroy(currentCard.gameObject);
            currentCard = null;
        }
    }

    public void executeCard() {
        if (currentCard != null) {
            onCardExecute?.Invoke(currentCard);
            CardGenerator.generateRandomCard();
        }
    }

    public void saveCard() {
        onCardSave?.Invoke(currentCard);
    }
}