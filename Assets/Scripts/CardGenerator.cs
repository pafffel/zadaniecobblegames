﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class CardGenerator {
    private static List<string> titles = new List<string>() {"Title1", "Title2", "Title3", "Title4", "Title5"};
    private static List<string> descriptions = new List<string>() {"Description 1", "Description 2", "Description 3", "Description 4", "Description 5"};
    private static List<Sprite> sprites;
    private static List<Effect> effects;

    public delegate void OnCardGenerated(CardData cardData);

    public static event OnCardGenerated cardGeneratedDelegate;

    public static void initialize() {
        loadSprites();
        loadEffects();
    }

    private static void loadSprites() {
        Sprite[] cardSprites = Resources.LoadAll<Sprite>("CardSprites/");
        sprites = cardSprites.ToList();
    }

    private static void loadEffects() {
        Effect[] cardEffects = Resources.LoadAll<Effect>("Effects/");
        effects = cardEffects.ToList();
    }

    public static void generateRandomCard() {
        string cardTitle = getRandomTitle();
        string cardDescription = getRandomDescription();
        Sprite cardSprite = getRandomSprite();
        Effect cardEffect = getRandomEffect();
        CardData newCardData = new CardData(cardTitle, cardDescription, cardSprite, cardEffect);
        newCardData.title = getRandomTitle();
        newCardData.description = getRandomDescription();
        newCardData.sprite = getRandomSprite();
        newCardData.effect = getRandomEffect();

        cardGeneratedDelegate?.Invoke(newCardData);
    }

    public static Sprite getSprite(string name) {
        return sprites.FirstOrDefault(x => x.name.Equals(name));
    }

    public static Effect getEffect(string name) {
        return effects.FirstOrDefault(x => x.name.Equals(name));
    }

    private static string getRandomTitle() {
        return titles[Random.Range(0, titles.Count)];
    }

    private static string getRandomDescription() {
        return descriptions[Random.Range(0, descriptions.Count)];
    }

    private static Sprite getRandomSprite() {
        return sprites[Random.Range(0, sprites.Count)];
    }

    private static Effect getRandomEffect() {
        return effects[Random.Range(0, effects.Count)];
    }
}