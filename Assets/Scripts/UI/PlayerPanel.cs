﻿using UnityEngine;

public class PlayerPanel : MonoBehaviour {
    public StatsBar healthBar, manaBar, speedBar;

    private int maxStatsValue = 100;
    private float _health;

    public float health {
        get => _health;
        set {
            _health = value;
            healthBar.setValue(_health / maxStatsValue);
        }
    }

    private float _mana;

    public float mana {
        get => _mana;
        set {
            _mana = value;
            manaBar.setValue(_mana / maxStatsValue);
        }
    }

    private float _speed;

    public float speed {
        get => _speed;
        set {
            _speed = value;
            speedBar.setValue(_speed / maxStatsValue);
        }
    }

    void Start() {
        CardDeck.onCardExecute += takeCardEffect;

        health = maxStatsValue;
        mana = maxStatsValue;
        speed = maxStatsValue;
    }

    private void takeCardEffect(Card card) {
        health += card.effect.healthDelta;
        mana += card.effect.manaDelta;
        speed += card.effect.speedDelta;

        health = health * card.effect.healthMultiply;
        mana = mana * card.effect.manaMultiply;
        speed = speed * card.effect.speedMultiply;
    }
}