﻿using UnityEngine;

public class ActionPanel : MonoBehaviour {
    public CardDeck cardDeck;

    public void generateNewCard() {
        CardGenerator.generateRandomCard();
    }

    public void saveCurrentCard() {
        cardDeck.saveCard();
    }

    public void executeCurrentCard() {
        cardDeck.executeCard();
    }
}