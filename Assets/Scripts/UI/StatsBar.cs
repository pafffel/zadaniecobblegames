﻿using UnityEngine;

public class StatsBar : MonoBehaviour {
    public RectTransform barFill;
    public RectTransform barBackground;

    public void setValue(float value) {
        Vector2 fillSizeDelta = barFill.sizeDelta;
        fillSizeDelta.x = barBackground.rect.size.x * value;
        barFill.sizeDelta = fillSizeDelta;
    }
}