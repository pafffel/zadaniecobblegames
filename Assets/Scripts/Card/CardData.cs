﻿using UnityEngine;

public struct CardData {
    public string title;
    public string description;
    public Sprite sprite;
    public Effect effect;

    public CardData(string cardTitle, string cardDescription, Sprite cardSprite, Effect cardEffect) {
        title = cardTitle;
        description = cardDescription;
        sprite = cardSprite;
        effect = cardEffect;
    }
}
