﻿using UnityEngine;
using UnityEngine.UI;

public class Card : MonoBehaviour {
    public Text title, description;
    public Image image;

    public Effect effect;

    public CardData cardData;
     
    public void initialize(CardData cardData) {
        this.cardData = cardData;
        title.text = cardData.title;
        description.text = cardData.description;
        image.sprite = cardData.sprite;
        effect = cardData.effect;
    }
}