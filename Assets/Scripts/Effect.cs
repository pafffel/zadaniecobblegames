﻿using UnityEngine;

[CreateAssetMenu(menuName = "Effects/New effect")]
public class Effect : ScriptableObject {
    [Header("Fixed value modificators")] public float healthDelta;
    public float manaDelta;
    public float speedDelta;

    [Header("Multiply modyficators")] public float healthMultiply = 1f;
    public float manaMultiply = 1f;
    public float speedMultiply = 1f;
}