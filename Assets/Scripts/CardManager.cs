﻿using System.Collections.Generic;
using SimpleJSON;
using UnityEngine;

public class CardManager : MonoBehaviour {
    private List<CardData> cards;

    void Start() {
        CardDeck.onCardSave += saveCard;
        loadCards();
    }

    private void loadCards() {
        cards = new List<CardData>();

        string loadedCardValue = PlayerPrefs.GetString($"card_{cards.Count}");

        while (!string.IsNullOrEmpty(loadedCardValue)) {
            JSONNode cardJson = JSON.Parse(loadedCardValue);
            Sprite cardSprite = CardGenerator.getSprite(cardJson["sprite"]);
            Effect cardEffect = CardGenerator.getEffect(cardJson["effect"]);
            CardData savedCardData = new CardData(cardJson["title"], cardJson["description"], cardSprite, cardEffect);
            cards.Add(savedCardData);
            loadedCardValue = PlayerPrefs.GetString($"card_{cards.Count}");
        }
    }

    void saveCard(Card card) {
        int newCardIndex = cards.Count;
        JSONNode cardJson = new JSONObject();
        cardJson["title"] = card.cardData.title;
        cardJson["description"] = card.cardData.description;
        cardJson["sprite"] = card.cardData.sprite.name;
        cardJson["effect"] = card.cardData.effect.name;
        cards.Add(card.cardData);
        PlayerPrefs.SetString($"card_{newCardIndex}", cardJson.ToString());
    }
}